(function() {
  'use strict';

  angular
    .module('zadminWeb')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'InterfaceController',
        controllerAs: 'main'
      });

    $urlRouterProvider.otherwise('/');
  }

})();
