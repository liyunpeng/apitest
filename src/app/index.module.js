(function() {
  'use strict';

  angular
    .module('zadminWeb', [
      'ngAnimate',
      'ngCookies',
      'ngResource',
      'ui.router',
      'ui.bootstrap',
      'hljs']);

})();
