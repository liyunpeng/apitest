(function() {
  'use strict';

  angular
    .module('zadminWeb')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
