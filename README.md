# ServiceTest

个人Web接口服务测试页面

支持普通Form表单请求

支持XHR请求

支持Restful形式请求

`运行时作为独立项目运行，因为涉及到跨域请求，请下载Chrome插件进行安装`

（https://chrome.google.com/webstore/detail/api-access/jegnjmcegcpodciadcoeneecmkiccfgi ）

或

（http://dl.iteye.com/topics/download/1ce24b89-120a-38dd-bcf2-b74b4d677588）


This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.12.1.


## Build & development

1.`npm install`

2.`bower install`

Run `gulp` for building and `gulp serve` for preview and `gulp serve:dist` for publish.

## Testing

Running `grunt test` will run the unit tests with karma.
